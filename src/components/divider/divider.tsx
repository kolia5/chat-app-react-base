import { IMessage } from '../../types/message-type';
import ItemsList from 'components/items-list/items-list';

import './divider.scss';

const getTimeSpans = (...args: IMessage[]): string[] => {

    const dateSpans: string[] = [];
    args.forEach(item => {
        // editedAt can be empty, so check it and if empty use createdAt
        if(item.editedAt){
            dateSpans.push(item.editedAt)
        } else {
            dateSpans.push(item.createdAt)
        }
    })
    const sortedDateSpans = dateSpans.sort((a, b) => new Date(a).getTime() - new Date(b).getTime());
    // delete dublicate
    const dateDivider = new Set(sortedDateSpans); 
    const arr = Array.from(dateDivider);
    return arr
}
const filterByDate = (items: IMessage[], date: string): IMessage[] => {
    const filtered = items.filter(item => {
        // editedAt can be empty, so check it and if empty use createdAt
        if(item.editedAt){
            return item.editedAt === date
        } else {
            return item.createdAt === date
        }
    })
    return filtered
}

interface DividerProps {
    messages: IMessage[];
    ownMessages: IMessage[];
}

const Divider: React.FC<DividerProps> = ({ messages, ownMessages }) => {
    // get time dividers from all messages
    const dividers = getTimeSpans(...messages, ...ownMessages);
    
    const sections: JSX.Element[] = dividers.map(date => {
        return (
            <div key={date}>
                <ul className="messages-divider">
                    <ItemsList 
                        messages={filterByDate(messages, date)} 
                        ownMessages={filterByDate(ownMessages, date)}
                    />
                </ul>
                <div className="date-label">
                    {date}
                </div>
            </div>
        )
    })
    
    return (
        <div className="divider">
            {sections}
        </div>
    )
}   
export default Divider;