import { useState, useEffect } from 'react';

import { IMessage } from 'types/message-type';

import Header from 'components/header/header';
import Preloader from 'components/preloader/preloader';
import ErrorMessage from 'components/error-message/error-message';
import MessageList from 'components/message-list/message-list';
import MessageInput from 'components/message-input/message-input';
import { generateUserId } from 'helpers/generate-user-id';

import './chat.scss';

interface ChatProps {
    url: string
}

const Chat: React.FC<ChatProps> = ({ url }) => {
    const [ messages, setMessages ] = useState<IMessage[]>([]);
    const [ ownMessages, setOwnMessages ] = useState<IMessage[]>([]);
    const [ isLoading, setIsLoading ] = useState<boolean>(true);
    const [ error, setError ] = useState<boolean>(false);
    const [ userId, setUserId ] = useState<string>('');

    const fetchData = async(url: string) => {
        try {
            const responce = await fetch(url);
            if(!responce.ok){
                throw new Error()
            }
            const data = await responce.json();
            setError(false);
            setMessages(data);
        } catch(error) {
            setError(true)
        } finally {
            setIsLoading(false)
        }
    }

    const addOwnMessages = (ownMessages: IMessage): void => {
        setOwnMessages(items => [...items, ownMessages])
    }

    useEffect(() => {
        const userId: string = generateUserId();
        setUserId(userId);
    }, [])

    useEffect(() => {
        fetchData(url)
    }, [url])

    if(error){
        return <ErrorMessage/>
    }

    return (
        <div className="chat">
            <Header messages={messages} ownMessages={ownMessages}/>
            {isLoading && <Preloader/>}
            {( messages.length > 0 || ownMessages.length > 0) && 
                <MessageList messageItems={messages} ownMessageItems={ownMessages}/>}
            <MessageInput addOwnMessage={addOwnMessages} userId={userId}/>
        </div>
    )
}
export default Chat;