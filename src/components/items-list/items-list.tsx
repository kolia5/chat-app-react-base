import Message from 'components/message/message';
import OwnMessage from 'components/own-message/own-message';
import { IMessage } from 'types/message-type';

interface ItemsListProps {
    messages: IMessage[];
    ownMessages: IMessage[];
}

const ItemsList: React.FC<ItemsListProps> = ({ messages, ownMessages }) => {
    const messageItems: JSX.Element[] = messages.map(message => {
        return (
            <li key={message.id}>
                <Message item={message}/>
            </li>
        )
            
        })
    const ownMessageItems: JSX.Element[] = ownMessages.map(ownMessage => {
        return (
            <li key={ownMessage.id}>
                <OwnMessage item={ownMessage}/>
            </li>
        )
    })
    return(
        <>
            {messages.length > 0 && messageItems}
            {ownMessages.length > 0 && ownMessageItems}
        </>
    )
}
export default ItemsList;




// const messagesList: JSX.Element[] = items.map(item => {
//     const itemId = item.id;
//     return (
//         <li key={itemId}>
//             <Message item={item}/>
//         </li>
//     )
// })
    

// return (
//     <div className="message-list">
//         { messagesList }
//         {/* <OwnMessage /> */}
//     </div>
// )