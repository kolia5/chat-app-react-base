import { library } from '@fortawesome/fontawesome-svg-core';
import { faHeart as fasFaHeart, faPen, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { faHeart as farFaHeart } from '@fortawesome/free-regular-svg-icons';

library.add(fasFaHeart, faPen, faTrashAlt, farFaHeart);
