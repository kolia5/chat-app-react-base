import './preloader.scss';

const Preloader: React.FC = () => {
    return (
        <div className="preloader">
            <div className="loadingio-spinner-dual-ring-ow9lzgwd1ra">
                <div className="ldio-7jctbi1igtl">
                    <div>

                    </div>
                    <div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Preloader;
