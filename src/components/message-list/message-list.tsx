import {IMessage } from 'types/message-type';

import Divider from 'components/divider/divider';

interface MessageListProps {
    messageItems: IMessage [];
    ownMessageItems: IMessage[];
}

const MessageList: React.FC<MessageListProps> = ({ messageItems, ownMessageItems }) => {
    return <Divider messages={messageItems} ownMessages={ownMessageItems}/>
}
export default MessageList;