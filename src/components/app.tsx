import Chat from 'components/chat/chat';

import 'components/app.css';

const baseApi = 'https://edikdolynskyi.github.io/react_sources/messages.json';

const App: React.FC = () => (
     <Chat url={baseApi}/>
)
export default App;