import { IMessage } from 'types/message-type';

import { LikeIcon } from 'components/icons';
import { dateToHHMM } from 'helpers/date-formating';

import './message.scss';

interface MessageProps {
    item: IMessage
}

const Message: React.FC<MessageProps> = ({item}) => {
    return (
        <div className="message">
            <div className="one">
                <div className="img-container">
                    <img src={item.avatar} alt="" className="message-user-avatar" />
                </div>
            </div>
            <div className="two">
                <p className="message-text">
                    {item.text}
                </p>
                <div className="message-info">
                    <span className="message-user-name">
                        {item.user}
                    </span>
                    <span className="message-time">
                        {item.editedAt ? dateToHHMM(item.editedAt) : dateToHHMM(item.createdAt)}
                    </span>
                </div>
            </div>
            <div className="three">
                <LikeIcon />
            </div>
        </div>
    )
}
export default Message;