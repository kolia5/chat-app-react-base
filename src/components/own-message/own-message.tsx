import { DeleteIcon, EditIcon } from 'components/icons';
import { IMessage } from 'types/message-type';

import { dateToHHMM } from 'helpers/date-formating';

import './own-message.scss';

interface OwnMessageProps {
    item: IMessage;
}

const OwnMessage: React.FC<OwnMessageProps> = ({ item }) => {
    return (
        <div className="own-message">
            <div className="one">
                <div className="img-container">
                    <img src={item.avatar} alt="" className="message-user-avatar" />
                </div>
            </div>
            <div className="two">
                <p className="message-text">
                    {item.text}
                </p>
                <div className="message-info">
                    <span className="message-user-name">
                        {item.user}
                    </span>
                    <span className="message-time">
                        {item.editedAt ? dateToHHMM(item.editedAt) : dateToHHMM(item.createdAt)}
                    </span>
                </div>
            </div>
            <div className="three">
                <DeleteIcon />
                <EditIcon />
            </div>
        </div>
    )
}
export default OwnMessage;