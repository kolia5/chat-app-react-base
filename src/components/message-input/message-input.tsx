import { useState, useEffect } from 'react';

import { generateOwnMessage } from 'helpers/generate-own-message';
import { AttachIcon } from 'components/icons';

import './message-input.scss';
import { IMessage } from 'types/message-type';

interface MessageInputProps {
    userId: string;
    addOwnMessage: (object: IMessage) => void;
}

const MessageInput: React.FC<MessageInputProps> = ({ addOwnMessage, userId }) => {
    const [ inputValue, setInputValue ] = useState<string>('');

    const handleChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
        setInputValue(event.target.value); 
    }
    const handleButtonClick: React.MouseEventHandler<HTMLButtonElement> = () => {
        const ownMessage = generateOwnMessage(inputValue, userId);
        // pass own message through func to component above
        addOwnMessage(ownMessage);
        // clear input;
        setInputValue('');
    }
    return (
        <div className="message-input">
            <AttachIcon/>
            <input 
                type="text" 
                className="message-input-text" 
                placeholder="Type your message ..."
                value={inputValue}
                onChange={handleChange}
            />
            <button 
                className="message-input-button" 
                type="submit"
                onClick={handleButtonClick}
            >Send</button>
        </div>
    )
}
export default MessageInput;