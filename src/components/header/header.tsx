import { LogoIcon } from 'components/icons';
import { dateToFull } from 'helpers/date-formating';
import { IMessage } from 'types/message-type';

import './header.scss';

interface HeaderProps {
    messages: IMessage[];
    ownMessages: IMessage[];
}

const Header: React.FC<HeaderProps> = ({ messages, ownMessages }) => {
    const allMessages: IMessage[] = [...messages, ...ownMessages];
    const allMessagesCount: number = allMessages.length;
    let allUsersId: Set<string> = new Set(); 
    // add id to set
    allMessages.forEach(message => allUsersId.add(message.userId));
    const allUsersCount = allUsersId.size;
    // search last message
    const allDates: string[] = [];
    allMessages.forEach(item => {
        // editedAt can be empty, so check it and if empty use createdAt
        if(item.editedAt){
            allDates.push(item.editedAt)
        } else {
            allDates.push(item.createdAt)
        }
    })
    const sortedDate = allDates.sort((a, b) => new Date(a).getTime() - new Date(b).getTime());
    const lastMessageDate: string = sortedDate[sortedDate.length - 1];
    
    return (
        <div className="header">
            <div className="logo">
                <LogoIcon/>
            </div>
            <div className="main-info">
                <div className="header-title">
                    My chat ua
                </div>
                <span className="header-users-count">
                    {allUsersCount} users
                </span>
                <span className="header-messages-count">
                    {allMessagesCount} messages
                </span>
            </div>
            <div className="header-last-message-date">
                last message at: {dateToFull(lastMessageDate)}
            </div>
        </div>
    )

}
export default Header;