const dateToHHMM = (date: string): string => {
    const format = new Date(date).toLocaleTimeString('en-gb',{ hour: 'numeric', minute: 'numeric' })
    return format;
}
const dateToFull = (date: string): string => {
    const format = new Date(date).toLocaleTimeString('en-gb',{
        day: '2-digit',
	    month: '2-digit',
	    year: 'numeric',
	    hour: 'numeric',
	    minute: 'numeric'
    })
    return format;
}

export { dateToHHMM, dateToFull }
