import { IconName } from '@fortawesome/fontawesome-common-types';

const  IconNames = {
    like: 'fasFaHeart' as IconName,
    liked: 'farFaHeart' as IconName,
    edit: 'faPen' as IconName,
    delete: 'faTrashAlt' as IconName
}
  
export { IconNames };


// const  IconNames = {
//     like: 'fasFaHeart' as IconName,
//     liked: 'farFaHeart' as IconName,
//     edit: 'faPen' as IconName,
//     delete: 'faTrashAlt' as IconName
// }